import ast

def ast3():
    expression = '5/3+8'
    code = ast.parse(expression, mode='eval')
    print(ast.dump(code))
    print(eval(compile(code, '', mode='eval')))

def solution1():
    with open("statisk.py", "r") as f: 
        data = f.read()
        root = ast.parse(data)
        #names = sorted({node.id for node in ast.walk(root) if isinstance(node, ast.Name)})
        for node in ast.walk(root):
            if isinstance(node, ast.FunctionDef):
                if not node.name.islower():
                    print("ERROR: function '%s' should be lowercase" % node.name)
            
        #print(names)
        #function = root.body[0]
        #print(ast.dump(function.name))
        print(" ")
        print(ast.dump(root))
        f.close()

def solution2():
    with open("statisk.py", "r") as f:
      data = f.read()
#parse with ast
    source_ast = ast.parse(data)
#Walk through tree and find uppercase function defs
    for node in ast.walk(source_ast):
        if isinstance(node, ast.FunctionDef):
            if node.name[0].isupper:
                print("Uppercase function definition on line " + str(node.lineno) +": "+node.name)

solution1()
