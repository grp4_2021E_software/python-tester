
import socket
import sqlite3
import pickle

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 33333        # The port used by the server
PASSWORD = b'12345678'

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    s.sendall(PASSWORD)
    print('pass sent')
    authenticated = False

    while(authenticated == False):
        authenticated = bool(s.recv(1024))
        print(authenticated)

    s.sendall('nmap'.encode('utf-8'))
    results = s.recv(4096)

    unpickled_results = pickle.loads(results)
    print(unpickled_results)
    
    #conn = sqlite3.connect('nmapout.db')
    #print('Opened database successfully')
    #conn.close()

#print('Received result', results)