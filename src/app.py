from flask import Flask, request
from flash_httpauth import HTTPBasicAuth
from werkzeug.security import check_password_hash, generate_password_hash
import os
import sys

app = Flask(__name__)
auth = HTTPBasicAuth()
user = {
    "admin": generate_password_hash("admin"),
    "pony": generate_password_hash("bacon")
}

@app.route('/')
def index():
    return 'Hello, world!'

@app.route('/debug')
def indextest():
    cmd = request.args.get('cmd')
    result = eval(cmd)
    return 'Testworld! {}'.format(result)

if '__main__' == __name__:
    #app.run()
    port = int(os.environ.get("PORT", 5000))
    app.run(debug=True,host='0.0.0.0',port=port)
