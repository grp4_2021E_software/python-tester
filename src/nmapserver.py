from socket import *
from datetime import datetime

import nmap3
import pickle


HOST = '127.0.0.1'
PORT = 33333        # The port used by the server
PASSWORD = b'12345678'

serverSocket = socket(AF_INET, SOCK_STREAM)
serverSocket.bind((HOST,PORT))
serverSocket.listen(1)
print('listening')
while 1:
    connectionSocket, addr = serverSocket.accept()
    recievedPassword = connectionSocket.recv(1024)
    print(recievedPassword)
    if (recievedPassword == PASSWORD):
        print('password correct')
        connectionSocket.sendall(b'1')
    recievedCommand = connectionSocket.recv(1024)
    if(recievedCommand == b'nmap'):
        nmap = nmap3.Nmap()
        results = nmap.scan_top_ports("localhost")
        pickled_results = pickle.dumps(results)
        connectionSocket.sendall(pickled_results)